// scrollRight
$.fn.extend({
    scrollRight: function (val) {
        if (val === undefined) {
            return this[0].scrollWidth - (this[0].scrollLeft + this[0].clientWidth) + 1;
        }
        return this.scrollLeft(this[0].scrollWidth - this[0].clientWidth - val);
    }
});

var counterLeft = 0;
var counterRight = 0;
var forwardsDirection = 1;
var backDirection = -1;
var isLeft = false;
var isRight = false;
// how long user have to scroll left/right before history back / forward
var threshold = 25;
// when to clear counters (in ms)
var resetCounterThreshold = 800;
// animations disabled?
var enableAnimation = true;

// sloth to dont shake browsers content if user accidently scrolls horizontal
var sloth = 5;

var isDebug = true;

var currentDirection = false;
var htmlBody = $("body");
var navDiv = '<div id="navDiv"></div>';
var leftNav = '<div id="leftNav" class="swipeNav swipeNavLeft"></div>';
var rightNav = '<div id="rightNav" class="swipeNav swipeNavReft"></div>';

var unload = false;

// timeOutFunction
var timeOut = false;

var options;

// apply options
function applyOptions() {
    chrome.storage.sync.get({
        animationsEnabled: true,
        reverseEnabled: false,
		threshold: 25
    }, function(items) {
    	debug("options: ");
    	debug(items);

        enableAnimation = items.animationsEnabled;
        enableReverse = items.reverseEnabled;
        threshold = items.threshold;

        if (enableAnimation) {
            $(window).on("beforeunload", animateBrowserPaging);
        }
        if (enableReverse) {
			forwardsDirection = -1;
			backDirection = 1;
        }
    });
}

// listen for changes in options
chrome.storage.onChanged.addListener(function(changes, namespace) {
    applyOptions();
});


$(document).scroll(function() {
	var body = $("body");
	var left = body.scrollLeft();
	var right = body.scrollRight();
	
	debug("scrollLeft: "+left);
	debug("scrollRight: "+right);
	if (left == 0) {
		debug("i am left!");
		isLeft = true;
	} else {
		isLeft = false;
		counterLeft = 0;
	}
	
	if (right == 0) {
		counterRight = true;
	} else {
		isRight = true;
		counterRight = 0;
	}
});


var timeOutFunc = function() {
	debug("timeout called");
	if (counterLeft > sloth || counterRight > sloth) {
		reset();
		return;
	}

	timeOut = setTimeout(timeOutFunc, resetCounterThreshold);
};

$(document).on('mousewheel', swipe);

function swipe(event) {
		if ((isLeft || isRight) && Math.abs(event.deltaX) > 0) {

			// back
			debug("deltaX: "+ event.deltaX)
			if (event.deltaX < 0 && isLeft) {
				debug("left "+counterLeft);
				if (counterLeft > threshold) {
					$(document).off("mousewheel");
					debug("going back");
					counterLeft = 0;

					currentDirection = backDirection;

					history.go(backDirection);
					setTimeout(function() {reattachEvent(backDirection);}, 500);
				}

				if (counterLeft >= sloth) {
					animateBody(-1);
				}

				counterLeft++;
			}

			if (event.deltaX > 0 && isRight) {
				debug("right "+counterRight);
				if (counterRight > threshold) {
					debug("going forward");
					$(document).off("mousewheel");
					counterRight = 0;

					currentDirection = forwardsDirection;

					history.go(forwardsDirection);
					setTimeout(function() {reattachEvent(forwardsDirection);}, 500);
				}

				if (counterRight >= sloth) {
                    animateBody(1);
				}

				counterRight++;
			}

			if (timeOut == false) {
				timeOutFunc(true);
			}
		} else {
			clearTimeoutAdvanced();
		}
}

$(document).ready(function() {
	applyOptions();
	createNavLayout();
	$(document).scroll();
	htmlBody.addClass("touchpadSwipeAnimationCore");
});

function createNavLayout(){
	htmlBody.wrapInner(navDiv);
	htmlBody.prepend(leftNav);
	htmlBody.append(rightNav);
	navDiv = $("#navDiv");
	leftNav = $("#leftNav");
	// i tried to add this via a css-class, but it was flat out ignored, so had to do it here
	// leftNav.css('background-color', 'grey');
	// leftNav.css('float', 'left')
	// leftNav.css('height', '100%');
	// leftNav.css('position', 'fixed');
	rightNav = $("#rightNav");
	// i tried to add this via a css-class, but it was flat out ignored, so had to do it here
	// rightNav.css('background-color', 'grey');
	// rightNav.css('float', 'right')
	// rightNav.css('height', '100%');
	// rightNav.css('position', 'fixed');
}

function animateBrowserPaging() {
	if (currentDirection != false) {
        if (currentDirection > 0) {
            var animClass = "touchpadSwipeAnimateTransitionBrowserPagingOutgoingForward";
        } else {
            var animClass = "touchpadSwipeAnimateTransitionBrowserPagingOutgoingBack";
        }
        htmlBody.addClass(animClass);
    }
}

function reattachEvent(direction) {
	// reattach if not refreshing page
	if (!unload) {
		$(document).on("mousewheel", swipe);
		reset();
	}
}

function reset() {
	counterLeft = 0;
	counterRight = 0;
	debug("timeout: resetting...");
	animateBody(0, true);
	timeOut = false;
}

function animateBody(leftOrRight, force) {
	if ((enableAnimation && !$(htmlBody).is(':animated')) || force) {

		if (leftOrRight > 0) {
			// forwards
			debug('###### translateX('+ (-counterRight*10) +'px)');
			navDiv.css('transform', 'translateX('+ (-counterRight*10) +'px)');
			rightNav.css('width', (counterRight*10) +'px');
		} else if (leftOrRight < 0) {
            // backwards
			debug('###### translateX('+ (counterLeft*10) +'px)');
			navDiv.css('transform', 'translateX('+ (counterLeft*10) +'px)');
			leftNav.css('width', (counterLeft*10) +'px');
		} else {
			// reset
			debug('###### translate = null');
			navDiv.css('transform', 'translateX('+ 0 +'px)');
			rightNav.css('width', 0 +'px');
			leftNav.css('width', 0 +'px');
		}
	}
}

function clearTimeoutAdvanced() {
	clearTimeout(timeOut);
	timeOut = false;

}

function debug(str) {
	if (isDebug) {
		console.log(str);
	}
}