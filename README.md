# Swipe Navigation
This extension adds Edge-like multitouch swipe navigation to Firefox. If your touchpad allows two finger horizontal scroll, you can use that to navigate forward and backward in the browser.

This extension is based on [koseduhemak's TouchpadSwipe](https://github.com/koseduhemak/touchpadswipe) extension. The original extension wasn't updated for about a year and i missed some navigation indicator, so i decided to create my own extension with a few tweaks here and there.

## How to use

### FireFox
Go to the Firefox Add-ons [page]() and install the extension. 

## Contribute
Your welcome to contribute. I'm just a beginner with Javascript and CSS so i'm sure there is a lot that can be improved.